import { Component, createRef } from 'react';
import { logIn } from '../Helpers/LoginManager';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.performLogin = this.performLogin.bind(this);

        this.clientIdInput = createRef();
        this.clientSecretInput = createRef();
    }
    performLogin() {
        logIn(this.clientIdInput.current.value, this.clientSecretInput.current.value)
        this.props.checkLogin();
    }
    render() {
        return (
            <div className="text-center">
                <div className="card mt-4" style={{ maxWidth: '500px', margin: '0 auto' }}>
                    <div className="card-header">
                        Please Authenticate
                    </div>
                    <div className="card-body text-start">
                        <div className="mb-3">
                            <label for="client-id" className="form-label">Client ID</label>
                            <input type="text" ref={this.clientIdInput} className="form-control" id="client-id" placeholder="ABCD-EFGH-IJKL-MNOP" />
                        </div>
                        <div className="mb-3">
                            <label for="client-secret" className="form-label">Client Secret</label>
                            <input type="text" ref={this.clientSecretInput} className="form-control" id="client-secret" placeholder="ABCD-EFGH-IJKL-MNOP" />
                        </div>
                        <div>
                            <button className="btn btn-primary" onClick={this.performLogin}>Log In</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.props.setData(
            "Log In",
            []
        )
    }
}