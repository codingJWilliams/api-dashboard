import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import { Ajax } from '../../Components/Ajax';
import GridView from '../../Components/Tables/GridView';
import { getApi } from '../../Helpers/LoginManager';

export default class ScriptIndex extends Component {
    render() {
        return (
            <div>
                <Ajax
                    fetch={async () => {
                        return (await getApi().call("generic-crud/list", {type: "script"})).script;
                    }}

                    render={d => 
                        <GridView 
                            columns={[
                                {
                                    name: "ID",
                                    index: "id",
                                    render: (row) => <Link to={"/script/"+row.id}>{row.id}</Link>
                                },
                                {
                                    name: "Language",
                                    index: "language"
                                }
                            ]}
                            data={d}
                        />
                    }
                />
            </div>
        );
    }

    componentDidMount() {
        this.props.setData(
            "Scripts",
            [
                {
                    title: "New Script",
                    icon: faPlus,
                    href: "/script/create"
                }
            ],
            [
                {
                    name: "Home",
                    href: "/"
                },
                {
                    name: "Scripts",
                    active: true
                }
            ]
        )
    }
}