import { Ajax } from "../../Components/Ajax";
import GenericView from "../../Components/MVC/GenericView";
import { getApi } from "../../Helpers/LoginManager";
import SelectPlusButton from "../../Components/SelectPlusButton";
import { Component } from "react";

export default class ScriptView extends Component {
    render() {
        return <GenericView
            attribs={[
                {
                    name: "ID",
                    type: "readonly",
                    field: 'id'
                },
                {
                    name: "Script",
                    type: "code",
                    field: "script"
                },
                {
                    name: "Language",
                    type: "text",
                    field: "language"
                }
            ]}
            route="script"
            name="Script"
            setData={this.props.setData}
            defaultsOnCreate={{
                script: '',
                language: 'bash'
            }}
            extra={(model, isCreated, mdv) => {
                return <div className="card mt-2">
                    <div className="card-body">
                        <h4>Run script on worker...</h4>
                        <Ajax
                            fetch={async () => ((await getApi().call("generic-crud/list", { type: 'worker' })).worker)}

                            render={workers => (
                                <div>
                                    <SelectPlusButton
                                        value={workers[0].id}
                                        options={workers.map(w => ({ value: w.id, name: w.hostname }))}
                                        button={onClick => (
                                            <button class="btn btn-primary" onClick={onClick}>Run</button>
                                        )}
                                        action={async value => {
                                            console.log(value);
                                            this.scriptoutput.style.display = "block";
                                            this.scriptoutput.value = "Running...";
                                            const res = await getApi().call("tasq/runscript", { script: model.id, worker: value });
                                            this.scriptoutput.value = res.output;
                                        }}
                                    />
                                    <textarea style={{display: 'none'}} className="form-control" rows="20" ref={inst => { this.scriptoutput = inst; }} />
                                </div>
                            )}
                        />
                    </div>
                </div>
            }}

        />
    }
}