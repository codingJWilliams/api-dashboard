import {Component} from 'react';

export default class Home extends Component {
    render() {
        return (
            <h1>Admin Dashboard</h1>
        );
    }

    componentDidMount() {
        this.props.setData(
            "Dashboard",
            []
        )
    }
}