import GenericView from "../../Components/MVC/GenericView";

export default function WorkerView(props) {
    return <GenericView
        attribs={[
            {
                name: "ID",
                type: "readonly",
                field: 'id'
            },
            {
                name: "Hostname",
                type: "text",
                field: "hostname"
            },
            {
                name: "SSH Port",
                type: "text",
                field: "ssh_port"
            }
        ]} 
        route="worker"
        name="Worker"
        endpoint="tasq/worker"
        setData={props.setData}
        defaultsOnCreate={{
            hostname: '',
            ssh_port: 22
        }}
    />
}