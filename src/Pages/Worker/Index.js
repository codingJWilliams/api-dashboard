import { Component } from 'react';
import { Link } from 'react-router-dom';
//import { Link } from 'react-router-dom';
import { Ajax } from '../../Components/Ajax';
import GridView from '../../Components/Tables/GridView';
import { getApi } from '../../Helpers/LoginManager';

export default class WorkerIndex extends Component {
    render() {
        return (
            <div>
                <Ajax
                    fetch={async () => {
                        return (await getApi().call("tasq/worker/list")).workers;
                    }}

                    render={d => 
                        <GridView 
                            columns={[
                                {
                                    name: "ID",
                                    index: "id"
                                },
                                {
                                    name: "Hostname",
                                    index: "hostname",
                                    render: (row) => <Link to={"/worker/"+row.id}>{row.hostname}</Link>
                                },
                                {
                                    name: "Port",
                                    index: "ssh_port"
                                }
                            ]}
                            data={d}
                        />
                    }
                />
            </div>
        );
    }

    componentDidMount() {
        this.props.setData(
            "Workers",
            [],
            [
                {
                    name: "Home",
                    href: "/"
                },
                {
                    name: "Workers",
                    active: true
                }
            ]
        )
    }
}