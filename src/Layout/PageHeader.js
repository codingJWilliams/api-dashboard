import { Link } from "react-router-dom";

export default function PageHeader(props) {
    if (!props.breadcrumbs || !props.breadcrumbs.length) return null;
    return (
        <ol class="breadcrumb main-breadcrumb">
            {props.breadcrumbs.map(b => <li className={'breadcrumb-item' + (b.active ? ' active' : '')}>{b.active ? b.name : <Link to={b.href}>{b.name}</Link>}</li>)}
        </ol>
    );
}