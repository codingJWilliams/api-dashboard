//import logo from './logo.svg';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import 'bootstrap-icons/font/bootstrap-icons.css';
import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faHome, faServer } from '@fortawesome/free-solid-svg-icons';
import Routes from '../Routes';
import { isLoggedIn } from '../Helpers/LoginManager';
import Login from '../Pages/Login';
import PageHeader from './PageHeader';
import "../Bootstrap/custom.scss";
import './App.scss';


function SidebarItem(props) {
    if (props.separator) {
        return (
            <li className="separator">
                {props.title}
            </li>
        )
    }
    if (props.href) {
        return (
            <li>
                <Link to={props.href}>
                    <FontAwesomeIcon icon={props.icon} />
                    <span>{props.title}</span>
                </Link>
            </li>
        )
    }
    if (props.onClick) {
        return <li>
            <a href="javascript:null" onClick={props.onClick}>
                <FontAwesomeIcon icon={props.icon} />
                <span>{props.title}</span>
            </a>
        </li>
    }
}

function getSidebarItems(items) {
    var out = [];
    for (const item of items) {
        if (item.subitems && item.subitems.length) {
            out.push({
                title: item.title,
                separator: true
            })
            out = out.concat(getSidebarItems(item.subitems));
        } else {
            out.push(item);
        }
    }
    return out;
}

function Sidebar(props) {
    return (
        <div className="sidebar-ct">
            <div className="sidebar">
                <div className="sidebar-brand">
                    <h5>JayWilliams API</h5>
                </div>
                <div className="sidebar-items">
                    <div className="page-title">
                        {props.pageTitle}
                    </div>
                    <ul className="sidebar-ul">
                        {getSidebarItems(props.items).map(i => <SidebarItem key={i.title} {...i} />)}
                    </ul>
                </div>
                <div className="sidebar-footer text-center">
                    <small>Copyright Jay Williams 2020</small>
                </div>
            </div>
        </div>
    );
}

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            pageTitle: "Loading...",
            breadcrumbs: [],
            loggedIn: isLoggedIn()
        }

        this.setData = this.setData.bind(this);
        this.checkLogin = this.checkLogin.bind(this);
    }

    setData(pageTitle, items, breadcrumbs = []) {
        this.setState({
            pageTitle,
            items,
            breadcrumbs
        });
    }

    checkLogin() {
        var loggedIn = isLoggedIn();
        if (loggedIn !== this.state.loggedIn) {
            this.setState({
                loggedIn
            })
        }
    }

    render() {
        if (this.state.loggedIn) {
            var items = this.state.items.concat([
                {
                    title: "Administration",
                    subitems: [
                        {
                            title: "Dashboard",
                            href: "/",
                            icon: faHome
                        },
                        {
                            title: "Users",
                            href: "/users",
                            icon: faUser
                        }
                    ]
                },
                {
                    title: "Tasks",
                    subitems: [
                        {
                            title: "Workers",
                            href: "/worker/index",
                            icon: faServer
                        },
                        {
                            title: "Scripts",
                            href: "/script/index",
                            icon: faServer
                        }
                    ]
                }
            ]);
        } else {
            items = [];
        }

        return (
            <Router>
                <div className="container-fluid">
                    <div className="row major-row">
                        <div className="col-md-2 sidebar-column">
                            <Sidebar items={items} pageTitle={this.state.pageTitle} />
                        </div>
                        <div className="col-md-10 main-page">
                            <PageHeader breadcrumbs={this.state.breadcrumbs} />
                            {this.state.loggedIn
                                ?
                                <Switch>
                                    {Routes.map(r => {
                                        var RouteView = r[1];
                                        return (
                                            <Route exact path={r[0]} key={r[0]}>
                                                <RouteView setData={this.setData}></RouteView>
                                            </Route>
                                        )
                                    })}
                                </Switch>
                                :
                                <Login setData={this.setData} checkLogin={this.checkLogin}></Login>
                            }
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
