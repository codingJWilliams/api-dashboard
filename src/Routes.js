import Home from './Pages/Home';
import ScriptIndex from './Pages/Script/Index';
import WorkerIndex from './Pages/Worker/Index';
import WorkerView from './Pages/Worker/View';
import ScriptView from './Pages/Script/View';

const routes = [
    [ "/", Home ],
    [ "/worker/index", WorkerIndex ],
    [ "/worker/:id", WorkerView ],
    [ "/script/index", ScriptIndex ],
    [ "/script/:id", ScriptView ]
];

export default routes