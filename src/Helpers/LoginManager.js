import { Api } from "./Api";

var api = null;

if (localStorage.getItem("clientid")) {
    api = new Api(localStorage.getItem("clientid"), localStorage.getItem("clientsecret"));
}

export function isLoggedIn() {
    return api !== null;
}
export function getApi() {
    return api;
}
export function logIn(clientid, clientsecret) {
    localStorage.setItem("clientid", clientid);
    localStorage.setItem("clientsecret", clientsecret);
    api = new Api(clientid, clientsecret);
}