import axios from 'axios';
import sjcl from 'sjcl'

export class Api {
    constructor(clientid, clientsecret) {
        this.clientid = clientid
        this.clientsecret = clientsecret;
        this.endpoint = "https://api.jaywilliams.me";
    }

    static sha256(text) {
        const myBitArray = sjcl.hash.sha256.hash(text)
        return sjcl.codec.hex.fromBits(myBitArray)
    }

    async call(method, params = {}) {
        var key = this.clientid;
        var verify = Math.round(Date.now()/1000).toString();
        var secret = this.clientsecret;
        var token = Api.sha256(secret+verify);
        try {
            const res = await axios.get(method+'.php', {
                baseURL: this.endpoint,
                params: {
                    key,
                    verify,
                    token,
                    ...params
                }
            });
            return res.data;
        } catch (e) {
            return { 
                success: false,
                error: e.message
            };
        }
    }
}