import { createRef } from "react";
import { Component } from "react";
import MonacoEditor from "react-monaco-editor";

export default class CodeEditor extends Component {
    constructor(props) {
        super(props);

        //this.state = {
        //    value: this.props.model[this.props.attrib.field]
        //}

        //this.onChange = this.onChange.bind(this);
        this.editorDidMount = this.editorDidMount.bind(this);
        this.monaco = createRef();
    }

    editorDidMount(editor, monaco) {
        editor.focus();
        editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_S, () => {
            this.props.save();
        })
    }

    /*onChange(newValue, e) {
        this.props.valueUpdate(this.props.attrib, newValue);
        this.setState({ value: newValue });
    }*/

    getValue() {
        //return "";
        return this.monaco.current.editor.getValue();
    }

    render() {
        return (
            <MonacoEditor
                language="shell"
                theme="vs-dark"
                height="500px"
                ref={this.monaco}
                width="90%"
                defaultValue={this.props.value}
                value={this.props.model[this.props.attrib.field].toString()}
                options={{ selectOnLineNumbers: true, automaticLayout: true }} 
                //onChange={this.onChange}
                editorDidMount={this.editorDidMount}
                //editorWillMount={this.editorWillMount}
            />
        );
    }
}