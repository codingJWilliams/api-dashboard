import { createRef } from "react";
import { Component } from "react";
import DetailView from "./DetailView";

export default class ModelDetailView extends Component {
    constructor(props) {
        super(props);

        this.state = props.model;

        //this.valueUpdate = this.valueUpdate.bind(this);
        this.save = this.save.bind(this);
        this.detailview = createRef();
    }

    /*valueUpdate(attrib, value) {
        this.setState({ [attrib.field]: value });
    }*/
    
    getModel() {
        return this.detailview.current.getModel();
    }

    save() {
        this.props.save(this.detailview.current.getModel());
    }

    render() {
        return (<DetailView model={this.state} ref={this.detailview} save={this.save} attribs={this.props.attribs} extra={(
            <tr>
                <td></td>
                <td>
                    <button className="btn btn-primary w-100" onClick={this.save}>{this.props.scenario === "create" ? "Create" : "Save"}</button>
                </td>
            </tr>
        )} />);
    }

}