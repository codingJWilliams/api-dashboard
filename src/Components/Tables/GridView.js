import './Tables.scss';

function GridViewHeader(props) {
    return (
        <thead>
            <tr>
                {props.columns.map((c, i) => (
                    <th key={i}>
                        {c.name}
                    </th>
                ))}
            </tr>
        </thead>
    )
}

function GridViewRow(props) {
    return (
        <tr>
            {props.columns.map((c, i) => (
                <td key={i}>
                    {c.render ? c.render(props.data) : props.data[c.index]}
                </td>
            ))}
        </tr>
    );
}

export default function GridView(props) {
    if (!props.data) return (<h1>No data</h1>);
    return (
        <table className="grid-view">
            <GridViewHeader columns={props.columns} />
            <tbody>
                {props.data.map((d, i) => <GridViewRow key={i} columns={props.columns} data={d} />)}
            </tbody>
        </table>
    )
}