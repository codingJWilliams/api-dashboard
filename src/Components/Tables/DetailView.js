import { Component, forwardRef } from 'react';
import './Tables.scss';
import CodeEditor from '../CodeEditor';
import { createRef } from 'react';

class DetailViewTextValue extends Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this);
        this.state = {
            value: this.props.model[this.props.attrib.field],
            valueUpdate: this.props.valueUpdate ? this.props.valueUpdate : () => { }
        }
    }

    onChange(e) {
        this.setState({ value: e.target.value });
        this.state.valueUpdate(this.props.attrib, e.target.value);
    }

    render() {
        return <input type="text" value={this.state.value} className="form-control" onChange={this.onChange} />
    }
}

const DetailViewValue = forwardRef((props, ref) => {
    if (props.attrib.type === "readonly") {
        if (!props.model[props.attrib.field]) return "Unknown";
        return <span ref={ref}>{props.model[props.attrib.field]}</span>;
    } else if (props.attrib.type === "text") {
        return <DetailViewTextValue ref={ref} {...props} />
    } else if (props.attrib.type === "code") {
        return <CodeEditor ref={ref} {...props} />
    }
})

const DetailViewRow = forwardRef((props, ref) => {
    return (
        <tr>
            <th>{props.attrib.name}</th>
            <td><DetailViewValue ref={ref} attrib={props.attrib} model={props.model} save={props.save} valueUpdate={props.valueUpdate} /></td>
        </tr>
    );
});

export default class DetailView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            model: this.props.model
        }

        this.refs_store = {};
        this.valueUpdate = this.valueUpdate.bind(this);
    }

    getModel() {
        const model = {...this.state.model};
        for(const key in this.refs_store) {
            if (!this.refs_store.hasOwnProperty(key)) continue;
            if (this.refs_store[key] && this.refs_store[key].current && this.refs_store[key].current.getValue) {
                model[key] = this.refs_store[key].current.getValue();
                console.log(key);
            }
        }
        this.setState({ model });
        return model;
    }

    valueUpdate(attrib, value) {
        this.setState({ model: { ...this.state.model, [attrib.field]: value } });
    }

    render() {
        var rows = [];
        for (const attrib of this.props.attribs) {
            if (!this.refs_store[attrib.field]) {
                const ref = createRef();
                this.refs_store[attrib.field] = ref;
            }
            
            rows.push(<DetailViewRow ref={this.refs_store[attrib.field]} key={attrib.field} attrib={attrib} model={this.state.model} valueUpdate={this.valueUpdate} save={this.props.save ? this.props.save : () => {}} />);
            console.log(this.refs_store)

        }
        return (
            <table className="detail-view">
                <tbody>
                    {rows}
                    {this.props.extra}
                </tbody>
            </table>
        )
    }
}