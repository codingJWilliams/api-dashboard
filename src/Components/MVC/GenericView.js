import { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { Ajax } from '../../Components/Ajax';
//import GridView from '../../Components/Tables/GridView';
import { getApi } from '../../Helpers/LoginManager';
import { faArrowLeft, faTrash } from '@fortawesome/free-solid-svg-icons';
import ModelDetailView from '../../Components/Tables/ModelDetailView';

export default withRouter(class GenericView extends Component {
    render() {
        const { id } = this.props.match.params;
        return (
            <div>
                <Ajax
                    fetch={async () => {
                        if (id === "create") return this.props.defaultsOnCreate;
                        return (await getApi().call("generic-crud/get", { id, type: this.props.route }))[this.props.route];
                    }}

                    ref={inst => { this.ajax = inst; }}

                    render={(d, key) =>
                    (
                        <div>
                            <ModelDetailView
                                attribs={this.props.attribs}

                                key={key}

                                scenario={id === "create" ? "create" : "edit"}

                                ref={inst => { this.mdv = inst; }}

                                save={async model => {
                                    if (id === "create") {
                                        const new_id = (await getApi().call("generic-crud/create", { model, type: this.props.route }))[this.props.route].id;
                                        this.props.history.push("/" + this.props.route + "/" + new_id);
                                        await this.ajax.reload()
                                        return;
                                    }
                                    await getApi().call("generic-crud/update", { model, type: this.props.route })
                                    await this.ajax.reload();
                                }}

                                model={d}
                            />
                            {this.props.extra(d, id !== "create", this.mdv)}
                        </div>
                    )
                    }
                />
            </div>
        );
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.setData(
            "scripts",
            [
                {
                    title: "Back",
                    icon: faArrowLeft,
                    href: "/" + this.props.route + "/index"
                },
                {
                    title: "Delete",
                    icon: faTrash,
                    onClick: async () => {
                        await getApi().call("generic-crud/delete", { id, type: this.props.route })
                        this.props.history.push("/" + this.props.route + "/index");
                    }
                }
            ],
            [
                {
                    name: "Home",
                    href: "/"
                },
                {
                    name: this.props.name,
                    href: "/" + this.props.route + "/index"
                },
                {
                    name: id === "create" ? "Create" : "Edit",
                    active: true
                },
            ]
        )
    }
})