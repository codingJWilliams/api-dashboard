import { Component } from "react";

export default class SelectPlusButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        }
        this.onChange = this.onChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onChange(e) {
        this.setState({ value: e.target.value });
    }

    onClick() {
        this.props.action(this.state.value)
    }

    render() {
        return (
            <div className="input-group mb-3">
                <select className="form-control" value={this.state.value} onChange={this.onChange}>
                    {this.props.options.map(o =>
                        <option key={o.value} value={o.value}>{o.name}</option>
                    )}
                </select>
                {this.props.button(this.onClick)}
            </div>
        )
    }
}