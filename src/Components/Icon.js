export default function Icon(props) {
    return (
        <i className={'bi-'+props.name}></i>
    );
}