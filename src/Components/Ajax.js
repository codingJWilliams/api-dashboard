import { Component } from "react";
import { Loader } from "./Loader/Loader";

export class Ajax extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            rerender: Date.now()
        }

        this.fetch = this.fetch.bind(this);
    }

    render() {
        console.log("Rerendering with state", this.state);
        return (
            <div>
                {!this.state.data ? (this.props.loader ? this.props.loader : (
                    <div style={{ width: '100%', textAlign: 'center' }}>
                        <Loader style={{ margin: "60px" }} />
                    </div>
                )) : this.props.render(this.state.data, this.state.rerender)}
            </div>
        );
    }

    async fetch() {
        const data = await this.props.fetch()
        this.setState({
            data
        })
    }

    reload() {
        return new Promise((resolve, reject) => {
            this.setState({ data: null, rerender: Date.now() }, () => {
                this.fetch().then(resolve);
            });
        })
    }

    componentDidMount() {
        this.fetch();
    }
}